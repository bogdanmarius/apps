from flask import Flask, render_template, request

import socket
import os

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/test_ports', methods=['POST'])
def test_ports():
    host = request.form['host']
    ports = request.form['ports'].split(',')

    results = []
    for port in ports:
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.settimeout(1)  # Set a timeout for the connection attempt
            result = sock.connect_ex((host, int(port)))
            if result == 0:
                status = 'Port is Open'
            else:
                status = 'Port is Closed'
            results.append((port, status))
        except socket.error as e:
            results.append((port, 'Error: ' + str(e)))
        finally:
            sock.close()

    return render_template('results.html', host=host, results=results)

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, host='0.0.0.0', port=port)
