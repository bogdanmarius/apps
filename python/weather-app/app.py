# app.py
from flask import Flask, render_template, request
import requests
import socket
import os

app = Flask(__name__)

def get_weather(city):
    # Replace 'YOUR_API_KEY' with your actual API key
    url = f"http://api.openweathermap.org/data/2.5/weather?q={city}&appid=YOUR_API_KEY"
    response = requests.get(url)
    data = response.json()
    return data

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        city = request.form['city']
        weather_data = get_weather(city)
        if weather_data['cod'] == 200:
            weather = {
                'city': city,
                'temperature': round(weather_data['main']['temp'] - 273.15, 2),
                'humidity': weather_data['main']['humidity'],
                'description': weather_data['weather'][0]['description'],
            }
        else:
            weather = None
        return render_template('index.html', weather=weather)
    return render_template('index.html')

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, host='0.0.0.0', port=port)
