## weather app

Create an account on https://openweathermap.org/ to get a free API key </br>

Make sure to replace 'YOUR_API_KEY' in the get_weather() function with your actual OpenWeatherMap API key. </br>