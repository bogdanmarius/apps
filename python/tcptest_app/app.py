from flask import Flask, render_template, request
import socket
import os

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        hostname = request.form['hostname']
        port = int(request.form['port'])
        result = test_port_connection(hostname, port)
        return render_template('index.html', result=result)
    return render_template('index.html')

def test_port_connection(hostname, port):
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(2)
        result = sock.connect_ex((hostname, port))
        sock.close()
        if result == 0:
            return f'Port {port} is OPEN on {hostname}'
        else:
            return f'Port {port} is CLOSED on {hostname}'
    except socket.error:
        return f'Could not connect to {hostname}'

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, host='0.0.0.0', port=port)