from django.shortcuts import render, get_object_or_404, redirect
from .models import Article

def article_list(request):
    articles = Article.objects.all()
    return render(request, 'myapp/article_list.html', {'articles': articles})

def article_detail(request, pk):
    article = get_object_or_404(Article, pk=pk)
    return render(request, 'myapp/article_detail.html', {'article': article})

def article_create(request):
    if request.method == 'POST':
        title = request.POST['title']
        content = request.POST['content']
        author = request.user
        Article.objects.create(title=title, content=content, author=author)
        return redirect('article_list')
    return render(request, 'myapp/article_form.html')

