import random
import string
import os
from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/generate_passwords', methods=['POST'])
def generate_passwords():
    num_passwords = 3
    min_length = 12
    max_length = 24

    passwords = []
    for _ in range(num_passwords):
        length = random.randint(min_length, max_length)
        password = ''.join(random.choices(string.ascii_letters + string.digits, k=length))
        passwords.append(password)

    return render_template('index.html', passwords=passwords)

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, host='0.0.0.0', port=port)
