import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class CurlRequestSender {
    private static volatile boolean running = true;

    public static void main(String[] args) {
        while (running) {
            try {
                // Use ProcessBuilder to execute the curl command
                ProcessBuilder processBuilder = new ProcessBuilder("curl", "https://localhost:80"); // Add the correct hostname to track
                processBuilder.redirectErrorStream(true);
                Process process = processBuilder.start();

                // Read the output of the curl command
                String output = readProcessOutput(process);

                // Save the output to a log file in JSON format
                saveOutputToJsonLog(output);

                // Wait for 2 seconds before sending the next request
                Thread.sleep(2000); // The value can be adjusted as needed
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static String readProcessOutput(Process process) throws IOException {
        StringBuilder output = new StringBuilder();
        int c;
        while ((c = process.getInputStream().read()) != -1) {
            output.append((char) c);
        }
        return output.toString();
    }

    private static void saveOutputToJsonLog(String output) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("logs/request_log.json", true))) {
            // Create a JSON object with the output and current timestamp
            String jsonLog = "{\"timestamp\": \"" + new Date() + "\", \"output\": \"" + output + "\"}";
            writer.write(jsonLog);
            writer.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void stop() {
        running = false;
    }
}
