const express = require('express');
const fs = require('fs');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
  fs.readFile('logs/request_log.json', 'utf8', (err, data) => {
    if (err) {
      res.send('Error reading log file');
    } else {
      res.send(data);
    }
  });
});

app.listen(port, () => {
  console.log(`Web server running at http://localhost:${port}`);
});
