<?php
// Check if a UDP port is open
function isPortOpen($host, $port)
{
    $socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
    socket_set_nonblock($socket);
    socket_connect($socket, $host, $port);
    socket_set_block($socket);
    $read = array($socket);
    $write = $except = null;
    $result = socket_select($read, $write, $except, 1);
    if ($result === false) {
        return false;
    } elseif ($result > 0) {
        return true;
    } else {
        return false;
    }
}

// Handle form submission
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $host = $_POST['host'];
    $ports = explode(',', $_POST['ports']);
    $results = array();

    foreach ($ports as $port) {
        $port = trim($port);
        if (!empty($port)) {
            $isOpen = isPortOpen($host, $port);
            $results[] = array(
                'port' => $port,
                'status' => $isOpen ? 'open' : 'closed'
            );
        }
    }
}
?>

<!DOCTYPE html>
<html>

<head>
    <title>UDP Port Tester</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }

        h1 {
            text-align: center;
        }

        form {
            width: 400px;
            margin: 0 auto;
        }

        input[type="text"] {
            width: 100%;
            padding: 8px;
            margin-bottom: 10px;
        }

        input[type="submit"] {
            width: 100%;
            padding: 8px;
            background-color: #4CAF50;
            color: white;
            border: none;
            cursor: pointer;
        }

        table {
            width: 400px;
            margin: 20px auto;
            border-collapse: collapse;
        }

        table td,
        table th {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        table th {
            background-color: #f2f2f2;
        }
    </style>
</head>

<body>
    <h1>UDP Port Tester</h1>
    <form method="post">
        <input type="text" name="host" placeholder="Host" required>
        <input type="text" name="ports" placeholder="Ports (comma-separated)" required>
        <input type="submit" value="Test Ports">
    </form>

    <?php if (isset($results)) : ?>
        <table>
            <thead>
                <tr>
                    <th>Port</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($results as $result) : ?>
                    <tr>
                        <td><?php echo $result['port']; ?></td>
                        <td><?php echo $result['status']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</body>

</html>