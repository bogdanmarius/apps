<?php
$error = false;
$result = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $host = $_POST['host'];
    $ports = explode(',', $_POST['ports']);

    foreach ($ports as $port) {
        $port = trim($port);
        if (!is_numeric($port) || $port < 1 || $port > 65535) {
            $error = true;
            $result = 'Invalid port(s) provided. Please enter valid port numbers.';
            break;
        }

        $connection = @fsockopen($host, $port, $errno, $errstr, 5);
        if ($connection) {
            $result .= "Port $port is open.\n";
            fclose($connection);
        } else {
            $result .= "Port $port is closed or unreachable.\n";
        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>TCP Port Tester</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }

        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-top: 10px;
        }

        textarea {
            width: 100%;
            height: 80px;
        }

        .error {
            color: red;
            margin-top: 10px;
        }

        .result {
            margin-top: 10px;
            white-space: pre-line;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>TCP Port Tester</h1>
        <form method="POST">
            <label>Host:</label>
            <input type="text" name="host" required>

            <label>Ports (comma-separated):</label>
            <textarea name="ports" required></textarea>

            <button type="submit">Test Ports</button>
        </form>

        <?php if ($error): ?>
            <div class="error"><?php echo $result; ?></div>
        <?php elseif ($result): ?>
            <div class="result"><?php echo $result; ?></div>
        <?php endif; ?>
    </div>
</body>
</html>