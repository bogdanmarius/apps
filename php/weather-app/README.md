## weather app

Create an account on https://openweathermap.org/ to get a free API key. </br>

Make sure to replace 'YOUR_API_KEY' in the $apiKey option with your actual OpenWeatherMap API key. </br>

Update the contents of the file city_list.json with the Cities you want to track. </br>
