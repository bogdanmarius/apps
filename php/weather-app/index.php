<?php
$apiKey = 'YOUR_OPENWEATHERMAP_API_KEY';

// Function to fetch weather data
function fetchWeatherData($city, $apiKey) {
    $url = 'http://api.openweathermap.org/data/2.5/weather?q=' . urlencode($city) . '&appid=' . $apiKey;
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    return json_decode($response, true);
}

// Get the selected city from the form
$selectedCity = isset($_POST['city']) ? $_POST['city'] : '';

// Fetch weather data for the selected city
if (!empty($selectedCity)) {
    $weatherData = fetchWeatherData($selectedCity, $apiKey);

    if ($weatherData && $weatherData['cod'] === 200) {
        $city = $weatherData['name'];
        $description = $weatherData['weather'][0]['description'];
        $temperature = round($weatherData['main']['temp'] - 273.15, 2); // Convert temperature from Kelvin to Celsius
        $humidity = $weatherData['main']['humidity'];
        $windSpeed = $weatherData['wind']['speed'];
    } else {
        $error = 'Failed to fetch weather data. Please try again later.';
    }
}

// Read the city list from the JSON file
$cityList = json_decode(file_get_contents('city_list.json'), true);
?>

<!DOCTYPE html>
<html>
<head>
    <title>Weather App</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }

        h1 {
            text-align: center;
        }

        form {
            text-align: center;
            margin-top: 20px;
        }

        .error {
            color: red;
        }
    </style>
</head>
<body>
    <h1>Weather App</h1>

    <?php if (isset($error)) : ?>
        <p class="error"><?= $error ?></p>
    <?php endif; ?>

    <form method="post" action="">
        <label for="city">Select a city:</label>
        <select name="city" id="city">
            <option value="">-- Select a city --</option>
            <?php foreach ($cityList as $cityItem) : ?>
                <option value="<?= $cityItem['name'] ?>" <?= $selectedCity === $cityItem['name'] ? 'selected' : '' ?>>
                    <?= $cityItem['name'] ?>
                </option>
            <?php endforeach; ?>
        </select>
        <button type="submit">Get Weather</button>
    </form>

    <?php if (isset($city)) : ?>
        <h2><?= $city ?></h2>
        <p><strong>Description:</strong> <?= $description ?></p>
        <p><strong>Temperature:</strong> <?= $temperature ?> &deg;C</p>
        <p><strong>Humidity:</strong> <?= $humidity ?>%</p>
        <p><strong>Wind Speed:</strong> <?= $windSpeed ?> m/s</p>
    <?php endif; ?>
</body>
</html>
