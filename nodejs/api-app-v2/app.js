const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios').default;
const cors = require('cors');
const winston = require('winston');

const app = express();
const PORT = process.env.PORT || 3000;

// Function to create a logger for each endpoint
const createLogger = (endpoint) => {
  return winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [new winston.transports.File({ filename: `logs/${endpoint}.log`, level: 'info' })],
  });
};

// Setup logger for the /health endpoint
const healthLogger = createLogger('health');

app.use(bodyParser.json());
app.use(cors());

app.get('/health', (req, res) => {
  healthLogger.info('Health Check Request Success');
  res.status(200).json({ status: 'OK' });
});

app.get('/chucknorris', async (req, res) => {
  const chuckLogger = createLogger('chuck');
  try {
    const response = await axios.get('https://api.chucknorris.io/jokes/random');
    chuckLogger.info('Chuck Norris API Request Success:', response.data);
    res.status(200).json({ chucknorris: response.data });
  } catch (error) {
    chuckLogger.error('Chuck Norris API Error:', error.message);
    res.status(500).json({ message: 'Error fetching Chuck Norris joke.' });
  }
});

app.get('/currency', async (req, res) => {
  const currencyLogger = createLogger('currency');
  try {
    const response = await axios.get('https://open.er-api.com/v6/latest/EUR');
    currencyLogger.info('Currency API Request Success:', response.data);
    res.status(200).json({ currency: response.data });
  } catch (error) {
    currencyLogger.error('Currency API Error:', error.message);
    res.status(500).json({ message: 'Error fetching currency data.' });
  }
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
