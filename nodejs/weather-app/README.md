<p>Nodejs application using OpenWeather API request

<br>Create an account on OpenWeather to get API key: https://openweathermap.org/ 

</p>

<b>START THE CONTAINER as desired:</b>
<br>in interactive mode:
<br>`docker-compose -f docker-compose.yaml up`

<br>in detached mode:
<br>`docker-compose -f docker-compose.yaml up -d`

<br>shutdown and clean:
<br>`docker-compose -f docker-compose.yaml down`</p>

<p><b>Usage</b>

<br>check weather:
<br>`http://localhost:3000/weather?city=City-name`

<br>health endpoint:
<br>`http://localhost:3000/health`

</p>
