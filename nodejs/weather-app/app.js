const express = require('express');
const axios = require('axios');

const app = express();
const port = process.env.PORT || 3000;

const apiKey = 'YOUR_API_KEY_HERE';

app.get('/weather', async (req, res) => {
  const city = req.query.city;
  try {
    const response = await axios.get(
      `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}`
    );
    const data = response.data;
    res.json(data);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred' });
  }
});

app.get('/health', (req, res) => {
  res.json({ status: 'OK - Running' });
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
