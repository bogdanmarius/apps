<p>Nodejs application using OpenWeather API request

<br>Create an account on OpenWeather to get API key: https://openweathermap.org/ </p>

<b>Start the application using the bash script</b> 
<br>This will create the logs directory and execute docker-compose -f docker-compose.yaml up -d

<b>Manual setup:</b>
<br>create the working directory in the same location as docker-compose.yaml file
<br>`mkdir logs`

in interactive mode:
<br>`docker-compose -f docker-compose.yaml up`

in detached mode:
<br>`docker-compose -f docker-compose.yaml up -d`

shutdown and clean:
<br>`docker-compose -f docker-compose.yaml down`</p>

<p><b>Usage</b>

check weather:
<br>`http://localhost:3000/weather?city=City-name`

health endpoint:
<br>`http://localhost:3000/health`

</p>
