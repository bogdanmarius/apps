const express = require('express');
const axios = require('axios');
const winston = require('winston');
const path = require('path');

// Define the absolute file system path to the logs directory
const logsDirectory = path.join(__dirname, 'logs');

// Create a Winston logger configuration to save logs to the directory
const logger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(
    winston.format.timestamp(), // Add timestamp to log entries
    winston.format.json()
  ),
  defaultMeta: { service: 'weather-app' },
  transports: [
    new winston.transports.File({ filename: path.join(logsDirectory, 'app.log') }),
  ],
});

const app = express();
const port = process.env.PORT || 3000;

const apiKey = 'YOUR_API_KEY_HERE';

app.get('/weather', async (req, res) => {
  const city = req.query.city;
  try {
    const response = await axios.get(
      `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}`
    );
    const data = response.data;
    res.json(data);
    logger.info(`Weather data fetched for city: ${city}`, { responseData: response.data });
  } catch (error) {
    res.status(500).json({ error: 'An error occurred' });
    logger.error(`Error fetching weather data for city: ${city}`);
  }
});

app.get('/health', (req, res) => {
  res.json({ status: 'OK - Running' });
  logger.info(`Application health: OK - Running `);
});

app.listen(port, () => {
  logger.info(`Server is running on port ${port}`);
});
