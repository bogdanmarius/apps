#!/bin/bash
# Author: Marius Bogdan Ursan
# Date: 08/11/2023
# Description Bash script to setup the application
# Version 1.0
# Date modified: 08/11/2023

echo 'Welcome to application setup script'
echo '====='

echo 'Creating working directory'
mkdir logs

echo 'Created working directory: logs'
echo '====='

echo 'Starting the docker container'
docker-compose -v
echo '====='
docker-compose -f docker-compose.yaml up -d

echo 'Setup and execution complete'
docker ps
echo 'To power down the container use: docker-compose -f docker-compose.yaml down'
