const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios').default;

const app = express();

app.use(bodyParser.json());


app.get('/chucknorris', async (req, res) => {
  try {
    const response = await axios.get('https://api.chucknorris.io/jokes/random');
    res.status(200).json({ chucknorris: response.data });
  } catch (error) {
    res.status(500).json({ message: 'Something went wrong.' });
  }
});

app.get('/currency', async (req, res) => {
  try {
    const response = await axios.get('https://open.er-api.com/v6/latest/EUR');
    res.status(200).json({ currency: response.data });
  } catch (error) {
    res.status(500).json({ message: 'Something went wrong.' });
  }
});

app.listen(3000);
