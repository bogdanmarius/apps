<p> Nodejs application </p>

<p> The application sends http requests to API endpoints and returns an reply. </p>

<p>Application endpoints:
<br>http://localhost:3000/chucknorris
<br>http://localhost:3000/currency </p>

<p>Postman should be used for a prettier output.</p>

<p>Integration with Apache Web server via reverse Proxy: 
https://gitlab.com/bogdanmarius/httpd-conf/-/tree/master/docker?ref_type=heads </p>
